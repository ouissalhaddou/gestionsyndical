﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SYNDICATA.Startup))]
namespace SYNDICATA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
