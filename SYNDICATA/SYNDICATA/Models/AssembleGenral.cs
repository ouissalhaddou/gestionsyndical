﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SYNDICATA.Models
{
    public class AssembleGenral
    {
        public int Id { get; set; }
        public string Motif { get; set; }
        public string Resume { get; set; }
        public string Convocation { get; set; }
        public DateTime Date { get; set; }
    }
}